package maestro.support.v1.svg;

import android.content.Context;
import android.graphics.*;
import android.graphics.drawable.Drawable;

public class SVG extends Drawable {

    public static final String TAG = SVG.class.getSimpleName();

    public static final String ROTATION = "Rotation";
    public static final String MUTATE_SCALE = "MutateScale";

    private Picture picture;
    private RectF bounds;
    private RectF limits = null;
    private ColorFilter mColorFilter;
    private float scale = 1f;
    private float mutateScale = 1f;
    private int alpha;

    private int rotateAngle = 0;

    SVG(Picture picture, RectF bounds) {
        this.picture = picture;
        this.bounds = bounds;
    }

    public SVG setScale(float scale) {
        this.scale = scale;
        return this;
    }

    public SVG setMutateScale(float mutateScale) {
        this.mutateScale = mutateScale;
        invalidateSelf();
        return this;
    }

    public SVG setScale(Context context) {
        return setScale(context.getResources().getDisplayMetrics().density);
    }

    void setLimits(RectF limits) {
        this.limits = limits;
    }

    public Bitmap getBitmap(float dpi) {
        final int width = Math.round(picture.getWidth() * dpi);
        final int height = Math.round(picture.getHeight() * dpi);
        Picture picture = getPicture();
        Bitmap bitmap = Bitmap.createBitmap(Math.round(width),
                Math.round(height), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        canvas.scale(dpi * mutateScale, dpi * mutateScale);
        canvas.drawPicture(picture);
        return bitmap;
    }

    public Bitmap getBitmap() {
        return getBitmap(scale);
    }

    public Picture getPicture() {
        return picture;
    }

    public RectF getLocalBounds() {
        return bounds;
    }

    public RectF getLocalLimits() {
        return limits;
    }

    @Override
    public int getIntrinsicWidth() {
        return picture != null ? Math.round(picture.getWidth() * scale) : -1;
    }

    @Override
    public int getIntrinsicHeight() {
        return picture != null ? Math.round(picture.getHeight() * scale) : -1;
    }

    public void setRotation(int angle) {
        rotateAngle = angle;
        invalidateSelf();
    }

    public int getRotation() {
        return rotateAngle;
    }

    @Override
    public void draw(Canvas canvas) {
        if (picture != null) {
            final int width = getIntrinsicWidth();
            final int height = getIntrinsicHeight();
            Rect bounds = getBounds();
            canvas.save();
            float difX = width - width * mutateScale;
            float difY = height - height * mutateScale;
            if (difX != 0 || difY != 0) {
                difX /= 2;
                difY /= 2;
            }
            canvas.rotate(rotateAngle, bounds.width() / 2, bounds.height() / 2);
            canvas.translate(bounds.left + difX, bounds.top + difY);
            canvas.scale(scale * mutateScale, scale * mutateScale);
            canvas.drawPicture(picture);
            canvas.restore();
        }
    }

    @Override
    public void setAlpha(int alpha) {
        this.alpha = alpha;
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        mColorFilter = cf;
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }

    public int getOriginWidth() {
        return picture.getWidth();
    }

    public int getOriginHeight() {
        return picture.getHeight();
    }

}
