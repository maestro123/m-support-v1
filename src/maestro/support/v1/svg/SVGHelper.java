package maestro.support.v1.svg;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.StateListDrawable;
import android.util.StateSet;
import android.view.View;
import android.widget.ImageView;

import java.util.HashMap;

/**
 * Created by Artyom on 10/29/2014.
 */
public class SVGHelper {

    public static float DPI;

    public static void init(Context context) {
        DPI = context.getResources().getDisplayMetrics().density;
    }

    public static SVGMenuItem getMenuDrawable(Resources resources, int resourceId, int replaceColor) {
        SVG svg = getDrawable(resources, resourceId, Color.BLACK, replaceColor, 1f);
        SVGMenuItem item = new SVGMenuItem(svg.getPicture(), svg.getLocalBounds());
        item.setScale(DPI);
        return item;
    }

    public static SVG getDrawable(Resources resources, int resourceId) {
        return SVGParser.getSVGFromResource(resources, resourceId).setScale(DPI);
    }

    public static SVG getDrawable(Resources resources, int resourceId, int searchColor, int replaceColor) {
        return SVGParser.getSVGFromResource(resources, resourceId, searchColor, replaceColor).setScale(DPI);
    }

    public static SVG getDrawable(Resources resources, int resourceId, float scale) {
        return SVGParser.getSVGFromResource(resources, resourceId).setScale(scale);
    }

    public static SVG getDrawable(Resources resources, int resourceId, int replaceColor) {
        return getDrawable(resources, resourceId, Color.BLACK, replaceColor);
    }

    public static SVG getDrawable(Resources resources, int resourceId, int searchColor, int replaceColor, float scale) {
        return SVGParser.getSVGFromResource(resources, resourceId, searchColor, replaceColor).setScale(scale);
    }

    public static void applySVG(ImageView imageView, int resourceId) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(getDrawable(imageView.getResources(), resourceId));
    }

    public static void applySVG(ImageView imageView, int resourceId, int searchColor) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(getDrawable(imageView.getResources(), resourceId, searchColor));
    }

    public static void applySVG(ImageView imageView, int resourceId, int searchColor, int replaceColor) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(getDrawable(imageView.getResources(), resourceId, searchColor, replaceColor));
    }

    public static void applySVG(ImageView imageView, int resourceId, float scale) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(getDrawable(imageView.getResources(), resourceId, scale));
    }

    public static void applySVG(ImageView imageView, int resourceId, int searchColor, int replaceColor, float scale) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(getDrawable(imageView.getResources(), resourceId, searchColor, replaceColor, scale));
    }

    public static void applySVG(ImageView imageView, SVG svg) {
        imageView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
        imageView.setImageDrawable(svg);
    }

    public static void prepareViewForSVG(View view) {
        if (view.getLayerType() != View.LAYER_TYPE_SOFTWARE)
            view.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
    }

    public static class SVGHolder {

        private HashMap<String, SVG> svgCache = new HashMap<String, SVG>();
        private HashMap<String, SVG> svgMenuCache = new HashMap<String, SVG>();

        private Resources resources;

        public SVGHolder(Resources resources) {
            this.resources = resources;
        }

        public void applySVG(ImageView imageView, int id, int color) {
            String key = new StringBuilder().append(id).append(color).toString();
            SVG svg = svgCache.get(id + color);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, color);
                svgCache.put(key, svg);
            }
            SVGHelper.applySVG(imageView, svg);
        }

        public void applySVG(ImageView imageView, int id) {
            String key = String.valueOf(id);
            SVG svg = svgCache.get(id);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id);
                svgCache.put(key, svg);
            }
            SVGHelper.applySVG(imageView, svg);
        }

        public void applySVG(ImageView imageView, int id, float scale) {
            String key = new StringBuilder().append(id).append(scale).toString();
            SVG svg = svgCache.get(id);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, scale);
                svgCache.put(key, svg);
            }
            SVGHelper.applySVG(imageView, svg);
        }

        public void applySVG(ImageView imageView, int id, int color, float scale) {
            String key = new StringBuilder().append(id).append(color).append(scale).toString();
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, Color.BLACK, color, scale);
                svgCache.put(key, svg);
            }
            SVGHelper.applySVG(imageView, svg);
        }

        public SVGMenuItem getMenuDrawable(int id, int color) {
            String key = new StringBuilder().append(id).append(color).toString();
            SVG svg = svgMenuCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getMenuDrawable(resources, id, color);
                svgMenuCache.put(key, svg);
            }
            return (SVGMenuItem) svg;
        }

        public SVG getDrawable(int id, int color) {
            String key = new StringBuilder().append(id).append(color).toString();
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, color);
                svgCache.put(key, svg);
            }
            return svg;
        }

        public SVG getDrawable(int id, int searchColor, int color) {
            String key = new StringBuilder().append(id).append(searchColor).append(color).toString();
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, searchColor, color);
                svgCache.put(key, svg);
            }
            return svg;
        }

        public SVG getDrawable(int id, int replaceColor, float scale) {
            String key = new StringBuilder().append(id).append(replaceColor).append(scale).toString();
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, Color.BLACK, replaceColor, scale);
                svgCache.put(key, svg);
            }
            return svg;
        }

        public SVG getDrawable(int id, float scale) {
            String key = new StringBuilder().append(id).append(scale).toString();
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, scale);
                svgCache.put(key, svg);
            }
            return svg;
        }

        public SVG getDrawable(int id) {
            String key = String.valueOf(id);
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id);
                svgCache.put(key, svg);
            }
            return svg;
        }

        public SVG getDrawable(int id, int searchColor, int replaceColor, float scale) {
            String key = new StringBuilder().append(String.valueOf(id + searchColor + replaceColor)).append("sc=").append(scale).toString();
            SVG svg = svgCache.get(key);
            if (svg == null) {
                svg = SVGHelper.getDrawable(resources, id, searchColor, replaceColor, scale);
                svgCache.put(key, svg);
            }
            return svg;
        }

    }

    public static StateListDrawable getStateListDrawable(SVGHelper.SVGHolder svgHolder, int resourceId, int normalColor, int activiatedColor) {
        StateListDrawable listDrawable = new StateListDrawable();
        listDrawable.addState(new int[]{android.R.attr.state_activated}, svgHolder.getDrawable(resourceId, activiatedColor));
        listDrawable.addState(StateSet.WILD_CARD, svgHolder.getDrawable(resourceId, normalColor));
        return listDrawable;
    }


}
