package maestro.support.v1.fview;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ProgressBar;

/**
 * Created by Artyom on 6/28/2015.
 */
public class FilterProgress extends ProgressBar implements IFilterObject {

    public static final String TAG = FilterProgress.class.getSimpleName();

    private int mPrimaryColor;
    private int mSecondaryColor;

    public FilterProgress(Context context) {
        super(context);
    }

    public FilterProgress(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public void setColors(int primary, int secondary) {
        mPrimaryColor = primary;
        mSecondaryColor = secondary;
        applyToDrawable(getDrawable());
    }

    @Override
    public void setProgressDrawable(Drawable d) {
        super.setProgressDrawable(d);
        applyToDrawable(d);
    }

    @Override
    public void setIndeterminateDrawable(Drawable d) {
        super.setIndeterminateDrawable(d);
        applyToDrawable(d);
    }

    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        applyToDrawable(getDrawable());
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        applyToDrawable(getDrawable());
    }

    private Drawable getDrawable() {
        return isIndeterminate() ? getIndeterminateDrawable() : getProgressDrawable();
    }

    private void applyToDrawable(Drawable drawable) {
        if (drawable != null) {
            drawable.setColorFilter(mPrimaryColor, PorterDuff.Mode.SRC_IN);
        }
    }

}
