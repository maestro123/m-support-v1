package maestro.support.v1.fview;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.RadioButton;

/**
 * Created by Artyom on 2/11/2015.
 */
public class FilterRadioButton extends RadioButton implements IFilterObject{

    private Drawable mButtonDrawable;
    private int mCheckedColor, mUnCheckedColor;

    public FilterRadioButton(Context context) {
        super(context);
    }

    public FilterRadioButton(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FilterRadioButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setColors(int checkedColor, int unCheckedColor) {
        mCheckedColor = checkedColor;
        mUnCheckedColor = unCheckedColor;
        applyColors();
    }

    @Override
    public void setButtonDrawable(Drawable d) {
        super.setButtonDrawable(d);
        mButtonDrawable = d;
        applyColors();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        applyColors();
    }

    private void applyColors(){
        if (mButtonDrawable != null) {
            mButtonDrawable.getCurrent().setColorFilter(isChecked() ? mCheckedColor : mUnCheckedColor, PorterDuff.Mode.SRC_IN);
            mButtonDrawable.invalidateSelf();
        }
    }

}
