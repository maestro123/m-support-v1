package maestro.support.v1.fview;

import android.content.Context;
import android.graphics.PorterDuff;
import android.util.AttributeSet;
import android.widget.Spinner;

/**
 * Created by Artyom on 2/11/2015.
 */
public class FilterSpinner extends Spinner implements IFilterObject{

    private int mCheckedColor, mUnCheckedColor;

    public FilterSpinner(Context context) {
        super(context);
    }

    public FilterSpinner(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public FilterSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public void setColors(int checkedColor, int unCheckedColor) {
        mCheckedColor = checkedColor;
        mUnCheckedColor = unCheckedColor;
        applyColor();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        applyColor();
    }

    private void applyColor(){
        if (getBackground() != null) {
            getBackground().getCurrent().setColorFilter(isPressed() ? mCheckedColor : mUnCheckedColor, PorterDuff.Mode.SRC_IN);
            getBackground().invalidateSelf();
        }
    }

}
