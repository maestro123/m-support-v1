package maestro.support.v1.fview;

import android.content.Context;
import android.graphics.PorterDuff;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.widget.AutoCompleteTextView;
import maestro.support.v1.R;

/**
 * Created by Artyom on 2/11/2015.
 */
public class FilterEditText extends AutoCompleteTextView implements IFilterObject {

    private int mCheckedColor, mUnCheckedColor;

    public FilterEditText(Context context) {
        super(context);
        init();
    }

    public FilterEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public FilterEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public void init() {
        setBackgroundResource(R.drawable.mtrl_edit_text);
    }

    @Override
    public void setColors(int checkedColor, int unCheckedColor) {
        mCheckedColor = checkedColor;
        mUnCheckedColor = unCheckedColor;
        applyColors();
    }

    @Override
    protected void drawableStateChanged() {
        super.drawableStateChanged();
        applyColors();
    }

    @Override
    protected void onFocusChanged(boolean focused, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(focused, direction, previouslyFocusedRect);
        applyColors();
    }

    private final void applyColors() {
        if (getBackground() != null) {
            getBackground().getCurrent().setColorFilter(isFocused() ? mCheckedColor : mUnCheckedColor, PorterDuff.Mode.SRC_IN);
            getBackground().invalidateSelf();
        }
    }
}
