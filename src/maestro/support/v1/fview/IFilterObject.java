package maestro.support.v1.fview;

/**
 * Created by Artyom on 3/12/2015.
 */
public interface IFilterObject {

    public void setColors(int primary, int secondary);

}
