package maestro.support.v1;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import java.util.ArrayList;

/**
 * Created by Artyom on 7/7/2015.
 */
public abstract class NavigationFragment extends Fragment implements AdapterView.OnItemClickListener {

    public static final String TAG = NavigationFragment.class.getSimpleName();

    private ListView mList;
    private NavigationAdapter mAdapter;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View v = inflater.inflate(R.layout.navigation_view, null);
        mList = (ListView) v.findViewById(R.id.list);
        mList.setDividerHeight(getDividerHeight());
        View adapterHeader = getHeader();
        if (adapterHeader != null) {
            mList.addHeaderView(adapterHeader);
        }
        mList.setOnItemClickListener(this);
        return v;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mList.setAdapter(mAdapter = new NavigationAdapter(getActivity(), getItems()));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        onActionItemClick(mAdapter.getItem(position - mList.getHeaderViewsCount()));
    }

    public int getDividerHeight(){
        return 0;
    }

    public abstract View getHeader();

    public abstract void onActionItemClick(ActionItem item);

    public abstract ArrayList<ActionItem> getItems();

    public abstract void applyIcon(NavigationAdapter.Holder holder, ActionItem item);

    public final class NavigationAdapter extends BaseAdapter {

        private Context mContext;
        private LayoutInflater mInflater;
        private ArrayList<ActionItem> mItems;

        public NavigationAdapter(Context context, ArrayList<ActionItem> items) {
            mContext = context;
            mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            mItems = items;
        }

        public void update(ArrayList<ActionItem> items){
            mItems = items;
            notifyDataSetChanged();
        }

        @Override
        public int getCount() {
            return mItems != null ? mItems.size() : 0;
        }

        @Override
        public ActionItem getItem(int position) {
            return mItems.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View v, ViewGroup parent) {
            Holder holder;
            if (v == null) {
                holder = new Holder(v = mInflater.inflate(R.layout.navigation_item_view, null));
            } else {
                holder = (Holder) v.getTag();
            }
            applyIcon(holder, mItems.get(position));
            Log.e(TAG, "title: " + mItems.get(position).getTitle());
            holder.Title.setText(mItems.get(position).getTitle());
            return v;
        }

        public class Holder {

            private ImageView Icon;
            private TextView Title;

            Holder(View view) {
                Icon = (ImageView) view.findViewById(R.id.icon);
                Title = (TextView) view.findViewById(R.id.title);
                view.setTag(this);
            }

        }

    }

    public static final class ActionItem {

        private String title;
        private int id = -1;
        private int iconResource;

        public ActionItem(int id, String title) {
            this.id = id;
            this.title = title;
        }

        public ActionItem(int id, String title, int iconResource) {
            this.id = id;
            this.title = title;
            this.iconResource = iconResource;
        }

        public int getId() {
            return id;
        }

        public String getTitle() {
            return title;
        }

        public int getIconResource() {
            return iconResource;
        }
    }

}
