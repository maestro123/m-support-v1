package maestro.support.v1.menu;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ObjectAnimator;
import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Handler;
import android.util.AttributeSet;
import android.view.*;
import android.view.animation.OvershootInterpolator;
import android.widget.PopupWindow;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by artyom on 16.4.15.
 */
public class MDropDownMenu extends ViewGroup {

    public static final String TAG = MDropDownMenu.class.getSimpleName();

    private final OvershootInterpolator mInterpolator = new OvershootInterpolator();

    private ArrayList<MDropItem> mItems = new ArrayList<>();
    private View mAnchorView;
    private TYPE mType = TYPE.VERTICAL;
    private ALIGN mAlign = ALIGN.CENTER;
    private ANIMATION mAnimation = ANIMATION.TRANSLATE;
    private ColorDrawable mBackgroundDrawable = new ColorDrawable(Color.parseColor("#80000000"));
    private PopupWindow mPopupWindow;
    private OnToggleStartListener mToggleListener;
    private OnItemClickListener mItemClickListener;
    private int mItemMargin = 0;
    private boolean isVisible = false;
    private boolean isAnchorAtLeft = false;
    private boolean isAnchorAtTop = false;

    public enum TYPE {
        HORIZONTAL, VERTICAL, BOTH
    }

    public enum ALIGN {
        CENTER, LEFT, RIGHT, TOP, BOTTOM
    }

    public enum ANIMATION {
        TRANSLATE, SCALE
    }

    public interface OnToggleStartListener {
        public void onToggleStart(boolean visible);
    }

    public interface OnItemClickListener {
        public void onItemClick(View v, MDropItem dropItem);
    }

    public MDropDownMenu(Context context) {
        super(context);
        init();
    }

    public MDropDownMenu(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    final void init() {
        mBackgroundDrawable.setAlpha(0);
        setBackgroundDrawable(mBackgroundDrawable);
    }

    @Override
    public boolean dispatchKeyEvent(KeyEvent event) {
        if (mPopupWindow != null && event.getKeyCode() == 4) {
            toggle();
            return true;
        }
        return super.dispatchKeyEvent(event);
    }


    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        if (mAnchorView == null)
            return;

        ensureAnchorState();

        final int childCount = getChildCount();

        final Rect rect = new Rect();
        getGlobalVisibleRect(rect);

        final Rect anchorRect = new Rect();
        mAnchorView.getGlobalVisibleRect(anchorRect);

        //TODO: fix for not full screen state

        int anchorX = (int) mAnchorView.getX();
        int startX = isAnchorAtLeft && mType == TYPE.HORIZONTAL ? anchorX + mAnchorView.getMeasuredWidth() : anchorX;
        int anchorY = (int) mAnchorView.getY() + (anchorRect.top - (int) mAnchorView.getY() - rect.top);
        int startY = isAnchorAtTop && mType == TYPE.VERTICAL ? anchorY + mAnchorView.getMeasuredHeight() : anchorY;

        for (int i = 0; i < childCount; i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                int width = child.getMeasuredWidth();
                int height = child.getMeasuredHeight();

                int fX = 0;
                int fY = 0;

                if (mType == TYPE.HORIZONTAL) {
                    startX += isAnchorAtLeft ? mItemMargin : -mItemMargin;
                    if (mAlign == ALIGN.CENTER) {
                        fY = (mAnchorView.getMeasuredHeight() - height) / 2;
                    } else if (mAlign == ALIGN.TOP) {
                        fY = 0;
                    } else if (mAlign == ALIGN.BOTTOM) {
                        fY = mAnchorView.getMeasuredHeight() - height;
                    }
                } else if (mType == TYPE.VERTICAL) {
                    startY += isAnchorAtTop ? mItemMargin : -mItemMargin;
                    if (mAlign == ALIGN.CENTER) {
                        fX = (mAnchorView.getMeasuredWidth() - width) / 2;
                    } else if (mAlign == ALIGN.LEFT) {
                        fX = 0;
                    } else if (mAlign == ALIGN.RIGHT) {
                        fX = mAnchorView.getMeasuredWidth() - width;
                    }
                }

                child.layout(startX + fX - (!isAnchorAtLeft && mType == TYPE.HORIZONTAL ? width : 0),
                        startY + fY - (!isAnchorAtTop && mType == TYPE.VERTICAL ? height : 0),
                        startX + (!isAnchorAtLeft && mType == TYPE.HORIZONTAL ? 0 : width) + fX,
                        startY + (!isAnchorAtTop && mType == TYPE.VERTICAL ? 0 : height) + fY);

                if (!isAnchorAtTop && mType == TYPE.VERTICAL) {
                    width = -width;
                    height = -height;
                }

                if (!isAnchorAtLeft && mType == TYPE.HORIZONTAL) {
                    width = -width;
                }

                if (mType == TYPE.HORIZONTAL) {
                    startX += width;
                } else if (mType == TYPE.VERTICAL) {
                    startY += height;
                } else {
                    startX += width;
                    startY += height;
                }

                if (!isVisible)
                    initializeItem(i);

            }
        }

    }

    public void toggle() {
        animate(!isVisible);
    }

    private boolean isLastAnimateItem;

    public void animate(final boolean visible) {
        if (isVisible == visible)
            return;
        isVisible = visible;
        if (mToggleListener != null) {
            mToggleListener.onToggleStart(visible);
        }
        for (int i = 0; i < getChildCount(); i++) {
            isLastAnimateItem = i == getChildCount() - 1;
            final View child = getChildAt(i);
            ViewPropertyAnimator animator = child.animate();
            animator.alpha(visible ? 1f : 0f);

            if (mAnimation == ANIMATION.TRANSLATE) {
                animator.setInterpolator(mInterpolator);
                if (mType == TYPE.HORIZONTAL) {
                    int distance = getChildWidthByOffset(i) + mAnchorView.getMeasuredWidth();
                    animator.translationX(visible ? 0 : isAnchorAtLeft ? -distance : distance);
                } else if (mType == TYPE.VERTICAL) {
                    int distance = getChildHeightByOffset(i) + child.getMeasuredHeight();
                    animator.translationY(visible ? 0 : isAnchorAtTop ? -distance : distance);
                }
            } else if (mAnimation == ANIMATION.SCALE) {
                float scale = visible ? 1f : 0f;
                animator.scaleX(scale).scaleY(scale).setDuration(150)
                        .setStartDelay(50 * (visible ? i : getChildCount() - 1 - i));
            }

            animator.setListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationStart(Animator animation) {
                    super.onAnimationStart(animation);
                    if (visible)
                        child.setVisibility(VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    if (!visible) {
                        child.setVisibility(GONE);
                        if (mPopupWindow != null && isLastAnimateItem) {
                            mPopupWindow.dismiss();
                        }
                    }
                }
            });
            animator.start();
        }
        ObjectAnimator animator = ObjectAnimator.ofInt(mBackgroundDrawable, "alpha", visible ? 255 : 0);
        animator.start();
    }

    private final void initializeItem(int itemPosition) {
        final View v = getChildAt(itemPosition);
        v.setAlpha(0);
        int x = 0, y = 0;
        if (mAnimation == ANIMATION.TRANSLATE) {
            if (mType == TYPE.HORIZONTAL) {
                final int distance = getChildWidthByOffset(itemPosition) + mAnchorView.getMeasuredWidth();
                x = isAnchorAtLeft ? -distance : distance;
            } else if (mType == TYPE.VERTICAL) {
                final int distance = getChildHeightByOffset(itemPosition) + v.getMeasuredHeight();
                y = isAnchorAtTop ? -distance : distance;
            }
            v.setTranslationX(x);
            v.setTranslationY(y);
        } else if (mAnimation == ANIMATION.SCALE) {
            v.setScaleY(0f);
            v.setScaleX(0f);
        }
        v.setVisibility(GONE);
    }

    public final int getChildWidthByOffset(int offset) {
        int width = 0;
        for (int i = 0; i < offset; i++) {
            width += getChildAt(i).getMeasuredWidth();
        }
        return width;
    }

    public final int getChildHeightByOffset(int offset) {
        int height = 0;
        for (int i = 0; i < offset; i++) {
            height += getChildAt(i).getMeasuredHeight();
        }
        return height;
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        for (int i = 0; i < getChildCount(); i++) {
            final View child = getChildAt(i);
            if (child.getVisibility() != GONE) {
                measureChild(child, widthMeasureSpec, heightMeasureSpec);
            }
        }
    }

    public void setAnchorView(View v) {
        mAnchorView = v;
        if (v != null) {
            ensureAnchorState();
            requestLayout();
        }
    }

    public void setPopupWindow(PopupWindow window) {
        mPopupWindow = window;
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });
    }

    private void ensureAnchorState() {
        final Rect anchorRect = new Rect();
        mAnchorView.getGlobalVisibleRect(anchorRect);

        if (anchorRect.top == 0 && anchorRect.bottom == 0 && anchorRect.left == 0 && anchorRect.right == 0) {
            mAnchorView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    ensureAnchorState();
                    requestLayout();
                    mAnchorView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                }
            });
            return;
        }

        int screenWidth = getResources().getDisplayMetrics().widthPixels;
        int screenHeight = getResources().getDisplayMetrics().heightPixels;

        isAnchorAtTop = anchorRect.bottom < screenHeight / 2;
        isAnchorAtLeft = anchorRect.left < screenWidth / 2;

    }

    public void addItem(MDropItem item) {
        mItems.add(item);
        addView(item);
    }

    public void addItems(List<MDropItem> items) {
        mItems.addAll(items);
        for (MDropItem item : items) {
            addView(item);
        }
    }

    public void addView(MDropItem item) {
        final View v = item.makeView(getContext(), LayoutInflater.from(getContext()));
        addView(v);
        v.setTag(item);
        v.setOnClickListener(mClickListener);
    }

    public TYPE getType() {
        return mType;
    }

    public MDropDownMenu setType(TYPE type) {
        mType = type;
        requestLayout();
        return this;
    }

    public MDropDownMenu setAnimation(ANIMATION animation) {
        mAnimation = animation;
        return this;
    }

    public MDropDownMenu setAlign(ALIGN align) {
        mAlign = align;
        requestLayout();
        return this;
    }

    public ALIGN getAlign() {
        return mAlign;
    }

    public MDropDownMenu setItemMargin(int margin) {
        mItemMargin = margin;
        requestLayout();
        return this;
    }

    public void setOnToggleStartListener(OnToggleStartListener listener) {
        mToggleListener = listener;
    }

    public void setOnItemClickListener(OnItemClickListener listener) {
        mItemClickListener = listener;
    }

    public boolean isVisible() {
        return isVisible;
    }

    private OnClickListener mClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            MDropItem item = (MDropItem) view.getTag();
            if (item != null && mItemClickListener != null) {
                mItemClickListener.onItemClick(view, item);
            }
        }
    };

    public static class MDropItem {

        private int id;

        public MDropItem(int id) {
            this.id = id;
        }

        public int getId() {
            return id;
        }

        public View makeView(Context context, LayoutInflater inflater) {
            return null;
        }

        public View makeDescriptionView(LayoutInflater inflater) {
            return null;
        }

    }

    public static MDropDownMenu show(Activity activity, View anchor, ArrayList<MDropItem> items) {
        final MDropDownMenu mDropDown = new MDropDownMenu(activity);
        mDropDown.setAnchorView(anchor);
        mDropDown.addItems(items);
        mDropDown.show(activity);
        return mDropDown;
    }

    public static MDropDownMenu build(Activity activity, View anchor, ArrayList<MDropItem> items) {
        final MDropDownMenu mDropDown = new MDropDownMenu(activity);
        mDropDown.setAnchorView(anchor);
        mDropDown.addItems(items);
        return mDropDown;
    }

    public void show(Activity activity){
        final View decorView = activity.getWindow().getDecorView();
        final PopupWindow popupWindow = new PopupWindow();
        popupWindow.setWidth(decorView.getWidth());
        popupWindow.setHeight(decorView.getHeight());
        popupWindow.setContentView(this);
        popupWindow.setFocusable(true);
        this.setPopupWindow(popupWindow);
        popupWindow.showAtLocation(decorView, Gravity.NO_GRAVITY, 0, 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                toggle();
            }
        }, 150);
    }
}
