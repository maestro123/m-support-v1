package maestro.support.v1.menu;


import android.content.Context;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;
import maestro.support.v1.R;
import maestro.support.v1.svg.SVGHelper;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Artyom on 3/17/2015.
 */
public class MPopupMenu extends BaseAdapter implements AdapterView.OnItemClickListener, CheckBox.OnCheckedChangeListener {

    public static final String TAG = MPopupMenu.class.getSimpleName();

    private Context mContext;
    private SVGHelper.SVGHolder mSVGHolder;
    private LayoutInflater mInflater;
    private PopupWindow mPopupWindow;
    private ArrayList<MPopupMenuItem> mItems = new ArrayList<>();
    private OnMenuItemClickListener mListener;

    private ListView mList;

    private int mPadding;
    private int mPopupSideMargin;
    private int mPopupMaxWidth;
    private int mPopupMaxHeight;

    public MPopupMenu(Context context, SVGHelper.SVGHolder holder, List<MPopupMenuItem> items) {
        this(context, holder, items, -1);
        mPopupSideMargin = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 4, context.getResources().getDisplayMetrics());
    }

    public MPopupMenu(Context context, SVGHelper.SVGHolder holder, List<MPopupMenuItem> items, int styleResource) {
        mContext = context;
        mSVGHolder = holder;
        mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (styleResource != -1) {
            mPopupWindow = new PopupWindow(context, null, -1, styleResource);
        } else {
            mPopupWindow = new PopupWindow(context, null, android.R.attr.popupMenuStyle);
        }
        mList = new ListView(context, null, android.R.attr.popupMenuStyle);
        mList.setBackgroundColor(Color.WHITE);
        mList.setOnItemClickListener(this);
        mList.setAdapter(this);
        mPopupWindow.setContentView(mList);
        mPopupWindow.setInputMethodMode(PopupWindow.INPUT_METHOD_NOT_NEEDED);
        mPopupWindow.setOutsideTouchable(true);
        mItems.addAll(items);

        final Resources res = context.getResources();
        mPadding = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 8, mContext.getResources().getDisplayMetrics());
        mPopupMaxWidth = Math.max(res.getDisplayMetrics().widthPixels / 2, res.getDimensionPixelSize(R.dimen.def_popup_width));
        mPopupWindow.setWidth(measureContentWidth());

        final int animStyle = getAnimationStyle();
        if (animStyle != -1)
            mPopupWindow.setAnimationStyle(animStyle);

        final Drawable listSelector = getListSelector();
        if (listSelector != null)
            mList.setSelector(listSelector);
    }

    public void show(View anchor) {
        mPopupWindow.setHeight(measureContentHeight(anchor));

        Rect globalRect = new Rect();
        anchor.getGlobalVisibleRect(globalRect);

        int x = globalRect.right - mPopupWindow.getWidth() - mPopupSideMargin;
        if (x < 0) {
            x = globalRect.left - mPopupSideMargin;
        }
        int y = globalRect.top;
        if (y + mPopupWindow.getHeight() + mPopupSideMargin > mContext.getResources().getDisplayMetrics().heightPixels) {
            y = globalRect.bottom - mPopupWindow.getHeight() - mPopupSideMargin;
        }

        mPopupWindow.showAtLocation(anchor, Gravity.NO_GRAVITY, x, y);
    }

    public void dismiss() {
        mPopupWindow.dismiss();
    }

    public MPopupMenu setOnMenuItemClickListener(OnMenuItemClickListener listener) {
        mListener = listener;
        return this;
    }

    @Override
    public int getCount() {
        return mItems != null ? mItems.size() : 0;
    }

    @Override
    public MPopupMenuItem getItem(int position) {
        return mItems.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return mItems.get(position).Type.ordinal();
    }

    @Override
    public View getView(int position, View v, ViewGroup parent) {
        MPopupMenuItem item = getItem(position);
        switch (item.Type) {
            case CHECK_BOX:
                CheckBoxItemViewHolder checkBoxHolder;
                if (v == null || v.getTag() == null || !(v.getTag() instanceof CheckBoxItemViewHolder)) {
                    checkBoxHolder = new CheckBoxItemViewHolder();
                    v = mInflater.inflate(R.layout.popup_item_checkbox_view, null);
                    checkBoxHolder.TextView = (TextView) v.findViewById(R.id.title);
                    ((CheckBox) checkBoxHolder.TextView).setOnCheckedChangeListener(this);
                    onItemCreate(item, checkBoxHolder);
                    v.setTag(checkBoxHolder);
                } else {
                    checkBoxHolder = (CheckBoxItemViewHolder) v.getTag();
                }
                checkBoxHolder.TextView.setTag(item);
                checkBoxHolder.TextView.setText(item.Title);
                if (item.IconResource != -1) {
                    Drawable drawable = getIcon(item, mSVGHolder);
                    checkBoxHolder.TextView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    checkBoxHolder.TextView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }
                onItemPrepare(item, checkBoxHolder);
                break;
            case NORMAL:
                NormalItemViewHolder normalHolder;
                if (v == null || v.getTag() == null || !(v.getTag() instanceof NormalItemViewHolder)) {
                    normalHolder = new NormalItemViewHolder();
                    v = mInflater.inflate(R.layout.popup_item_view, null);
                    normalHolder.TextView = (TextView) v.findViewById(R.id.title);
                    onItemCreate(item, normalHolder);
                    v.setTag(normalHolder);
                } else {
                    normalHolder = (NormalItemViewHolder) v.getTag();
                }

                normalHolder.TextView.setText(item.Title);
                if (item.IconResource != -1) {
                    Drawable drawable = getIcon(item, mSVGHolder);
                    normalHolder.TextView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    normalHolder.TextView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }
                onItemPrepare(item, normalHolder);
                break;
            case DIVIDER:
                DividerItemViewHolder dividerHolder;
                if (v == null || v.getTag() == null || !(v.getTag() instanceof DividerItemViewHolder)) {
                    dividerHolder = new DividerItemViewHolder();
                    v = mInflater.inflate(R.layout.popup_divider_item_view, null);
                    dividerHolder.TextView = (TextView) v.findViewById(R.id.title);
                    dividerHolder.Line = v.findViewById(R.id.line);
                    onItemCreate(item, dividerHolder);
                    v.setTag(dividerHolder);
                } else {
                    dividerHolder = (DividerItemViewHolder) v.getTag();
                }

                dividerHolder.TextView.setText(item.Title);
                if (item.IconResource != -1) {
                    Drawable drawable = getIcon(item, mSVGHolder);
                    dividerHolder.TextView.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
                    dividerHolder.TextView.setCompoundDrawablesWithIntrinsicBounds(drawable, null, null, null);
                }
                onItemPrepare(item, dividerHolder);
                break;
            case CUSTOM:
                getCustomView(position, v, parent);
                break;
        }
        v.setPadding(v.getPaddingLeft(), position == 0 ? mPadding : 0, v.getPaddingRight(), position == getCount() - 1 ? mPadding : 0);
        return v;
    }

    @Override
    public boolean isEnabled(int position) {
        return getItem(position).Type != ItemType.DIVIDER;
    }

    public View getCustomView(int position, View v, ViewGroup parent) {
        return v;
    }

    public void onItemCreate(MPopupMenuItem item, ItemViewHolder holder) {

    }

    public void onItemPrepare(MPopupMenuItem item, ItemViewHolder holder) {

    }

    public Drawable getIcon(MPopupMenuItem item, SVGHelper.SVGHolder holder) {
        return holder.getDrawable(item.IconResource, getIconColor(item));
    }

    public int getIconColor(MPopupMenuItem item) {
        return Color.BLACK;
    }

    public Drawable getListSelector() {
        return new ColorDrawable(Color.TRANSPARENT);
    }

    public int getAnimationStyle() {
        return -1;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (mListener != null) {
            mListener.onMenuItemClick(getItem(position));
        }
        dismiss();
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        MPopupMenuItem item = (MPopupMenuItem) buttonView.getTag();
        if (mListener != null && item != null) {
            mListener.onMenuItemClick(item);
        }
        dismiss();
    }

    public class ItemViewHolder {
        public TextView TextView;
    }

    public final class NormalItemViewHolder extends ItemViewHolder {
    }

    public final class CheckBoxItemViewHolder extends ItemViewHolder {
    }

    public final class DividerItemViewHolder extends ItemViewHolder {
        View Line;
    }

    private int measureContentWidth() {
        int maxWidth = 0;
        View itemView = null;
        int itemType = 0;
        FrameLayout mMeasureParent = null;

        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(maxWidth, View.MeasureSpec.UNSPECIFIED);
        final int count = getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(mContext);
            } else {
                mMeasureParent.removeAllViews();
            }

            itemView = getView(i, itemView, mMeasureParent);
            mMeasureParent.addView(itemView);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemWidth = itemView.getMeasuredWidth();
            if (itemWidth >= mPopupMaxWidth) {
                return mPopupMaxWidth;
            } else if (itemWidth > maxWidth) {
                maxWidth = itemWidth;
            }
        }

        return maxWidth;
    }

    private int measureContentHeight(View anchor) {
        final int maxHeight = mPopupWindow.getMaxAvailableHeight(anchor);
        int height = 0;
        int itemType = 0;
        View itemView = null;
        FrameLayout mMeasureParent = null;

        final int widthMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int heightMeasureSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        final int count = getCount();
        for (int i = 0; i < count; i++) {
            final int positionType = getItemViewType(i);
            if (positionType != itemType) {
                itemType = positionType;
                itemView = null;
            }

            if (mMeasureParent == null) {
                mMeasureParent = new FrameLayout(mContext);
            }

            itemView = getView(i, itemView, mMeasureParent);
            mMeasureParent.removeAllViews();
            mMeasureParent.addView(itemView);
            itemView.measure(widthMeasureSpec, heightMeasureSpec);

            final int itemHeight = itemView.getMeasuredHeight();
            height += itemHeight;
            if (height > maxHeight) {
                return maxHeight;
            }
        }

        return height;
    }

    public static class MPopupMenuItem {

        public int Id;
        public String Title;
        public int IconResource = -1;
        public ItemType Type = ItemType.NORMAL;

        public MPopupMenuItem(String title, int iconResource) {
            Title = title;
            IconResource = iconResource;
        }

        public MPopupMenuItem(String title, int iconResource, ItemType type) {
            this(title, iconResource);
            Type = type;
        }

        public MPopupMenuItem(ItemType type) {
            Type = type;
        }

        public MPopupMenuItem(String title, ItemType type) {
            this(title, -1);
            Type = type;
        }

        public MPopupMenuItem setId(int id) {
            Id = id;
            return this;
        }

    }

    public enum ItemType {
        NORMAL, CHECK_BOX, DIVIDER, CUSTOM
    }

    public interface OnMenuItemClickListener {
        public void onMenuItemClick(MPopupMenuItem item);
    }

}